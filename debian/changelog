libfilter-template-perl (1.043-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libfilter-template-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 01:47:51 +0100

libfilter-template-perl (1.043-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 21:30:36 +0100

libfilter-template-perl (1.043-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 13:39:29 +0100

libfilter-template-perl (1.043-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Angel Abad ]
  * Email change: Angel Abad -> angel@debian.org

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright.
  * Update license stanzas.
  * Switch to "3.0 (quilt)" source format.
  * Bump debhelper compatibility level to 8.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Oct 2013 23:10:08 +0200

libfilter-template-perl (1.040-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Add myself to Uploaders and Copyright
  * Standards-Version 3.8.4 (drop perl version dep)
  * Use new DEP5 copyright format
  * Rewrite control description

  [ gregor herrmann ]
  * Change my email address.

  [ Angel Abad ]
  * Update my email address

  [ gregor herrmann ]
  * Remove dependency on libfilter-perl, Filter::Util::Call is in core.

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 14 Mar 2010 12:09:50 -0400

libfilter-template-perl (1.03-1) unstable; urgency=low

  * New upstream release
  * debian/control: add myself to uploaders
  * Standards-Version 3.8.2

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Thu, 30 Jul 2009 15:51:18 +0200

libfilter-template-perl (1.02-2) unstable; urgency=low

  [ Damyan Ivanov ]
  * [debian/watch] Stop capturing file extension

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser field
    (source stanza); Homepage field (source stanza). Removed: XS- Vcs-Svn
    fields. Moved: perl-modules to Build-Depends-Indep. Added: /me to
    Uploaders.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #467785)
    - update based on dh-make-perl's templates
    - don't install README any more (the same as the POD documentation)
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/copyright: add upstream source location.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 08 Mar 2008 14:01:46 +0100

libfilter-template-perl (1.02-1) unstable; urgency=low

  * Initial Release. (Closes: #401578)

 -- Gunnar Wolf <gwolf@debian.org>  Mon,  4 Dec 2006 10:27:27 -0600
